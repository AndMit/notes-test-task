//
//  NotesTableCell.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

class NotesTableCell: UITableViewCell {
    
    @IBOutlet weak var noteTextLabel: UILabel!
    
    func setNoteText(text: String) {
        self.noteTextLabel.text = text
        self.noteTextLabel.padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
}
