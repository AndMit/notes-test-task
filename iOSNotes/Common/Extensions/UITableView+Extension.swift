//
//  UITableView+Extension.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

// MARK: - Reusable cell method
extension UITableView {
    
    func reusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        let id = String(describing: T.self)
        return self.dequeueReusableCell(withIdentifier: id, for: indexPath) as! T
    }
    
    func reusableHeader<T>(_ reusableHeaderClass: T.Type) -> T? {
        let id = String(describing: reusableHeaderClass)
        return self.dequeueReusableHeaderFooterView(withIdentifier: id) as? T
    }
    
    func addSeparatorForLastCell() {
        let footerView: UIView? = self.tableFooterView
        var footerFrame: CGRect? = footerView?.frame
        footerFrame?.size.height = 0
        footerView?.frame = footerFrame ?? CGRect.zero
    }
    
}

