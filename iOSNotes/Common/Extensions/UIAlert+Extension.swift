//
//  UIAlert+Extension.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func alert(title: String? = nil, message: String? = nil) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    
    static func actionSheet(title: String? = nil, message: String? = nil) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
    }
    
    func addAction(title: String, style: UIAlertAction.Style = .default, handler: ((UIAlertAction) -> Void)?) {
        self.addAction(UIAlertAction.init(title: title, style: style, handler: handler))
    }
    
    
    
    func addCancelAction(title: String = "Cancel", handler: ((UIAlertAction) -> Void)? = nil) {
        self.addAction(title: title, style: .cancel, handler: handler)
    }
    
    func addDestructiveAction(title: String, handler: ((UIAlertAction) -> Void)? = nil) {
        self.addAction(title: title, style: .destructive, handler: handler)
    }
    
    func addDefaultAction(title: String, handler: ((UIAlertAction) -> Void)? = nil) {
        self.addAction(title: title, style: .default, handler: handler)
    }
    
}

extension UIViewController {
    
    func presentAlert(title: String? = nil, text: String? = nil, cancelButtonTitle: String = "ОК", cancelButtonHandler: (()->Void)? = nil) {
        
        let alert = UIAlertController.init(title: title, message: text, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { _ in
            cancelButtonHandler?()
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
}
