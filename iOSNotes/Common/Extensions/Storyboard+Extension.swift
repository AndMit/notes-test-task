//
//  Storyboard+Extension.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static func instantiate(nameOfStoryboard: String?) -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate(nameOfStoryboard: String? = nil) -> Self {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(self)
        
        // this splits by the dot and uses everything after, giving "MyViewController"
        let className = fullName.components(separatedBy: ".")[1]
        let storyboardName = className.components(separatedBy: "Controller")[0]
        // load our storyboard
        let storyboard = UIStoryboard(name: nameOfStoryboard ?? storyboardName, bundle: Bundle.main)
        
        // instantiate a view controller with that identifier, and force cast as the type that was requested
        return storyboard.instantiateViewController(withIdentifier: storyboardName) as! Self
    }
}

extension UITableViewController: Storyboarded {}
extension UIViewController: Storyboarded {}
