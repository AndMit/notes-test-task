//
//  NoteModel.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

class NoteModel {
    
    var id: Int
    var title: String
    
    init?(json: [String : Any]) {
        guard
            let id = json["id"] as? Int,
            let title = json["title"] as? String
            else {
                return nil
        }
        self.id = id
        self.title = title
    }
}
