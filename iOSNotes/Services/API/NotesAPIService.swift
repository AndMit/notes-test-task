//
//  NotesAPIService.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit
import Alamofire

enum NotesURLs {
    case common
    
    var stringValue: String {
        switch self {
        case .common:
            return "\(domain)"+"/notes"
        }
    }
}

var domain: String {
    return "http://private-9aad-note10.apiary-mock.com"
}

enum GeneralResult<T, E: Error> {
    case success(T)
    case error(E)
}

typealias VoidHandler = () -> Void
typealias ValueHandler<T: Any> = (T) -> Void
typealias APIResponseErrorHandler = (ApiResultError?) -> Void

enum ApiResultError: Error {
    case badRequest
    case noNetworkAvailAble
    case parsingError
    case wrongStatusCode(Int)
    case noResult
    case undefinded
}


class NotesAPIService {
    
    static func fetch(completion: @escaping (GeneralResult<[NoteModel], ApiResultError>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        
        manager.request(NotesURLs.common.stringValue, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (responseJSON) in
            
            switch responseJSON.result {
            case .success(let value):
                guard let jsonArray = value as? [[String: Any]] else {
                    completion(GeneralResult.error(.parsingError))
                    return
                }
                let notes = jsonArray.compactMap {NoteModel(json: $0)}
                completion(GeneralResult.success(notes))
            case .failure(let error):
                completion(GeneralResult.error(.undefinded))
                print(error)
            }
        }
    }
    
    static func create(completion: @escaping (GeneralResult<NoteModel, ApiResultError>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        
        manager.request(NotesURLs.common.stringValue, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (responseJSON) in
            
            switch responseJSON.result {
            case .success(let value):
                guard let json = value as? [String: Any] else {
                    completion(GeneralResult.error(.parsingError))
                    return
                }
                guard let note = NoteModel(json: json) else {
                    completion(GeneralResult.error(.parsingError))
                    return
                }
                completion(GeneralResult.success(note))
            case .failure(let error):
                completion(GeneralResult.error(.undefinded))
                print(error)
            }
        }
    }
    
    static func edit(id: Int, completion: @escaping (GeneralResult<NoteModel, ApiResultError>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        
        let url = "\(NotesURLs.common.stringValue)/"+"\(id)"
        
        manager.request(url, method: .put, parameters: nil, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (responseJSON) in
            
            switch responseJSON.result {
            case .success(let value):
                guard let json = value as? [String: Any] else {
                    completion(GeneralResult.error(.parsingError))
                    return
                }
                guard let note = NoteModel(json: json) else {
                    completion(GeneralResult.error(.parsingError))
                    return
                }
                completion(GeneralResult.success(note))
            case .failure(let error):
                completion(GeneralResult.error(.undefinded))
                print(error)
            }
        }
    }

    
    static func deleteBy(id: Int, completion: @escaping APIResponseErrorHandler) -> Void {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        let url = "\(NotesURLs.common.stringValue)/"+"\(id)"
        manager.request(url, method: .delete, parameters: nil, encoding: URLEncoding.default, headers: nil).validate().response { (response) in
            
            if let error = response.error {
                completion(.undefinded)
                print(error)
            } else {
                completion(nil)
            }
        }
    }
    
}

