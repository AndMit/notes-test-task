//
//  NotesModalController.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

class NotesModalController: UIViewController {
    
    var noteText: String?
    
    typealias SaveHandler = (NotesModalController) -> Void
    
    var saveDidTap: SaveHandler!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noteTextView.text = noteText
        
        let tapGestureReconizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        view.addGestureRecognizer(tapGestureReconizer)
    }
    
    @IBAction func saveButtonDidTap(_ sender: UIButton) {
        
        self.saveDidTap(self)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonDidTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func save(handler: SaveHandler?) {
        self.saveDidTap = handler
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}

extension NotesModalController: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.noteText = textView.text
    }
}
