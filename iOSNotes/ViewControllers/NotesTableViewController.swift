//
//  NotesTableViewController.swift
//  iOSNotes
//
//  Created by Andriy Mitin on 4/8/19.
//  Copyright © 2019 Reinto. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController {
    
    var modalController: NotesModalController {
        return NotesModalController.instantiate()
    }
    
    var notes: [NoteModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchNotes()
        self.tableView.addSeparatorForLastCell()
    }
    
    @IBAction func addButtonDidTap(_ sender: UIBarButtonItem) {
        let controller = self.modalController
        controller.noteText = ""
        self.present(controller, animated: true)
        
        controller.saveDidTap = { modalVC in
            self.createNote()
        }
    }
}

extension NotesTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = notes[indexPath.row]
        
        let controller = self.modalController
        controller.noteText = note.title
        
        controller.saveDidTap = { modalVC in
            self.editNote(id: note.id)
        }
        
        self.present(controller, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let id = self.notes[indexPath.row].id
            self.delete(id: id, completion: {
                self.notes.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
        }
        return [delete]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = notes[indexPath.row]
        let cell: NotesTableCell =  self.tableView.reusableCell(for: indexPath)
        cell.setNoteText(text: note.title)
        return cell
    }
}

//Requests
extension NotesTableViewController {
    private func fetchNotes() {
        NotesAPIService.fetch { [weak self] (result) in
            switch result {
            case .success(let notes):
                self?.notes = notes
                self?.tableView.reloadData()
            case .error(let error):
                self?.presentAlert(title: "Error", text: error.localizedDescription, cancelButtonTitle: "OK", cancelButtonHandler: nil)
            }
        }
    }
    
    private func editNote(id: Int) {
        NotesAPIService.edit(id: id) { [weak self] (result) in
            switch result {
            case .success(let note):
                self?.notes.forEach {
                    if $0.id == note.id {
                        $0.title = note.title
                    }
                }
                self?.tableView.reloadData()
            case .error(let error):
                self?.presentAlert(title: "Error", text: error.localizedDescription, cancelButtonTitle: "OK", cancelButtonHandler: nil)
            }
        }
    }
    
    private func createNote() {
        NotesAPIService.create { [weak self] (result) in
            switch result {
            case .success(let note):
                self?.notes.append(note)
                self?.tableView.reloadData()
            case .error(let error):
                self?.presentAlert(title: "Error", text: error.localizedDescription, cancelButtonTitle: "OK", cancelButtonHandler: nil)
            }
        }
    }
    
    private func delete(id: Int, completion: @escaping ()->()) -> Void {
        NotesAPIService.deleteBy(id: id) { [weak self] (error) in
            if let error = error {
                self?.presentAlert(title: "Error", text: error.localizedDescription, cancelButtonTitle: "OK", cancelButtonHandler: nil)
            } else {
                completion()
            }
        }
    }
    
    
}


